class CreateContacts < ActiveRecord::Migration[6.0]
  def change
    create_table :contacts do |t|
      t.string     :email,                 null: false
      t.string     :name,                  null: false
      t.string     :address,               null: false
      t.date       :birth_date,            null: false
      t.string     :phone,                 null: false
      t.string     :encrypted_credit_card, null: false
      t.string     :credit_card_last_part, null: false
      t.string     :franchise,             null: false
      t.references :user,                  null: false, foreign_key: true

      t.timestamps
    end
    add_index :contacts, %i[email user_id], unique: true
  end
end
