class AlterContactsFranchiseToEnum < ActiveRecord::Migration[6.0]
  def change
    remove_column :contacts, :franchise
    add_column :contacts, :franchise, :integer, default: 0
  end
end
