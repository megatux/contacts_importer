class CreateImportationProcesses < ActiveRecord::Migration[6.0]
  def change
    enable_extension 'hstore' unless extension_enabled?('hstore')

    create_table :importation_processes do |t|
      t.string :filename,     null: false
      t.hstore :fields_setup, null: false
      t.references :user,     null: false, foreign_key: true

      t.timestamps
    end
  end
end
