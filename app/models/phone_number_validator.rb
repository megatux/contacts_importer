class PhoneNumberValidator < ActiveModel::EachValidator
  FORMAT_1 = /^\(\+\d\d\) \d{3} \d{3} \d{2} \d{2}$/
  FORMAT_2 = /^\(\+\d\d\) \d{3}-\d{3}-\d{2}-\d{2}$/

  def validate_each(record, attribute, value)
    record.errors.add(attribute, 'must be a valid phone number') unless value.to_s =~ FORMAT_1 || value.to_s =~ FORMAT_2
  end
end
