class ImportationProcess < ApplicationRecord
  validates_presence_of :filename, :fields_setup, :user
  belongs_to :user
end
