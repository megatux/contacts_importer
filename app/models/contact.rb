class Contact < ApplicationRecord
  belongs_to :user
  enum franchise: %i[american_express diners_club discover jcb mastercard visa]

  attr_accessor :credit_card

  validates_presence_of :email, :name, :address, :birth_date, :phone, :credit_card, :user
  validates :email, length: { minimum: 5 }, format: { with: URI::MailTo::EMAIL_REGEXP, message: 'invalid email' }
  validates :phone, phone_number: { allow_blank: true }

  before_validation :encrypt_credit_card
  before_validation :get_last_credit_card_digits

  private

  def encrypt_credit_card
    self.encrypted_credit_card = Digest::SHA256.digest credit_card if credit_card.present?
  end

  def get_last_credit_card_digits
    self.credit_card_last_part = credit_card.last(4) if credit_card.present?
  end

  def get_franchise
    # TODO: fetch franchise correctly or set error
    self.franchise = Contact.visa
    # record.errors.add(:franchise, 'unknown franchise') unless ...
  end
end
