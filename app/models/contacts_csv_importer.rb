require 'csv'

class ContactsCsvImporter
  attr_reader :fields_setup

  def initialize(fields_setup)
    @fields_setup = fields_setup
  end

  def process_file(file_or_io)
    csv = CSV.new(file_or_io)
    csv.map do |line|
      process_line line
    end
  end

  def process_line(_line)
    contact = Contact.new(
      email: 'a@a.com',
      name: 'a',
      address: 'a',
      phone: '(+00) 123 321 11 22',
      birth_date: Time.zone.yesterday,
      credit_card: '30569309025904',
      user: User.new
    )
  end
end
