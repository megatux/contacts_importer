class HomeController < ApplicationController
  def index
    @user_email = current_user.email
    @contacts_count = current_user.contacts.count
    @importation_processes_count = current_user.importation_processes.count
  end
end
