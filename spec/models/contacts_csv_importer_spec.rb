require 'rails_helper'

RSpec.describe ContactsCsvImporter, type: :model do
  it 'process a file or io' do
    file_content = StringIO.new(
      "a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,\n" +
      "1,1,1,1,1,1,1,1,1,1,1,1,1,1\n"
    )

    importer = ContactsCsvImporter.new({})
    pp importer.process_file file_content
  end
end
