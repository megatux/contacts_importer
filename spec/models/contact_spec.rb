require 'rails_helper'

RSpec.describe Contact, type: :model do
  let(:user) { User.new(email: 'someone@company.com') }
  let(:valid_phone) { '(+00) 123 321 11 22' }
  let(:valid_credit_card_number) { '30569309025904' }

  context 'creating a contact' do
    it 'is invalid without all attributes set' do
      contact = Contact.new

      refute(contact.valid?)
      expect(contact.errors.messages.size).to eq 7
      expect(contact.errors.messages).to include(email: ["can't be blank", 'is too short (minimum is 5 characters)',
                                                         'invalid email'])
      expect(contact.errors.messages).to include(name: ["can't be blank"])
      expect(contact.errors.messages).to include(address: ["can't be blank"])
      expect(contact.errors.messages).to include(birth_date: ["can't be blank"])
      expect(contact.errors.messages).to include(phone: ["can't be blank"])
      expect(contact.errors.messages).to include(credit_card: ["can't be blank"])
      expect(contact.errors.messages).to include(user: ['must exist', "can't be blank"])
    end

    it 'is valid when all attributes are set' do
      contact = Contact.new(
        email: 'a@a.com',
        name: 'a',
        address: 'a',
        phone: valid_phone,
        birth_date: Time.zone.yesterday,
        credit_card: '123',
        user: user
      )
      expect(contact.valid?).to be_truthy
    end

    it 'reject invalid emails' do
      ['a.com', '@.com', 'pepe@', ' ', ' bob@it.co'].each do |email|
        c = Contact.new(email: email)
        c.validate
        expect(c.errors.messages[:email]).to include('invalid email')
      end
    end

    it 'accepts valid emails' do
      ['a@a.com', 'a@a.co', 'pepe_lopez@it'].each do |email|
        c = Contact.new(email: email)
        c.validate
        expect(c.errors.messages[:email]).to be_empty
      end
    end

    it 'validates phone field' do
      [
        '(+11) 999 888 66 55',
        '(+99) 999-888-66-55',
        '(+00) 000 000 00 00',
        '(+00) 000-000-00-00'
      ].each do |phone|
        c = Contact.new(phone: phone)
        c.validate
        expect(c.errors.messages[:phone]).to be_empty
      end

      [
        '(+11) 99 888 66 55',
        '(+9) 999-888-66-55',
        '(+00) 000 000 00 00 00',
        '000 000 00 00 00',
        '(+) 000 000 00 00 00',
        '(+00)',
        '123'
      ].each do |phone|
        c = Contact.new(phone: phone)
        c.validate
        expect(c.errors.messages[:phone]).not_to be_empty
      end
    end

    it 'fetchs the last 4 digits of the credit_card field' do
      c = Contact.new credit_card: valid_credit_card_number
      c.validate
      expect(c.credit_card_last_part).to eq('5904')
    end

    it 'generates a digest from the credit_card field' do
      c = Contact.new credit_card: valid_credit_card_number
      c.validate
      expect(c.encrypted_credit_card).to eq(Digest::SHA256.digest(c.credit_card))
    end

    xit 'extracts franchise'
  end
end
